<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    use HasFactory;
        protected $table = 'events';

    protected $fillable = [
        'ref',
        'name',
        'time',
        'stadium_id',
        'team1_id',
        'team2_id',
        'details'
    ];

    public function team1() {
        return $this->belongsTo(Team::class, 'team1_id', 'id');
    }
    public function team2() {
        return $this->belongsTo(Team::class, 'team2_id', 'id');
    }
    public function stadium() {
        return $this->belongsTo(Stadium::class, 'stadium_id', 'id');
    }
}

