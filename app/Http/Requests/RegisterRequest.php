<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'username' => 'required | min:3 |unique:users',
            'firstname' => 'required | min:3',
            'lastname' => 'sometimes',
            'email' => 'required | email | unique:users',
            'phone' => 'sometimes | numeric',
            'password' => 'required | confirmed'
        ];
    }
}