<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreEventRequest;
use App\Http\Requests\UpdateEventRequest;
use App\Models\Event;
use App\Models\Team;
use App\Models\Stadium;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $events = Event::all();
        return view('admin.event.list', compact('events'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $teams = Team::all();
        $stadiums = Stadium::all();
        return view('admin.event.create', compact('teams', 'stadiums'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreEventRequest $request)
    {
        $data = $request->validated();

        $query = [
            'ref' => $this->refGenerator(),
            'name' => $data['name']  ?? null,
            'time' => $data['date'],
            'team1_id' => $data['team1'],
            'team2_id' => $data['team2'],
            'stadium_id' => $data['stadium'],
            'details' => $data['details'] ?? null,
            'status' => 1,
        ];
        Event::create($query);

        return redirect()->route('admin.events.list');
    }

    /**
     * Display the specified resource.
     */
    public function show(Event $event)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Event $event)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateEventRequest $request, Event $event)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Event $event)
    {
        //
    }
}
