<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Event;

class ClientController extends Controller
{
    public function show()
    {
        $events = Event::all();

        return view('user.index', compact('events'));
    }
}
