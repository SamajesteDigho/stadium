<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    /**
     * Registration Page
     */
    public function register_page(Request $request)
    {
        return view('auth.register');
    }

    /**
     * Registration Acknowledgement
     */
    public function register(RegisterRequest $request)
    {
        $data = $request->validated();

        $query = [
            'username' => $data['username'],
            'firstname' => $data['firstname'],
            'lastname' => $data['lastname'] ?? null,
            'phone' => $data['phone'] ?? null,
            'email' => $data['email'],
            'avatar' => null,
            'role' => 'ADMIN',
            'status' => 'ACTIVE',
            'password' => bcrypt($data['password']),
        ];

        User::create($query);

        return redirect()->route('login');
    }

    /**
     * Login Page
     */
    public function login_page(Request $request)
    {
        return view('auth.login');
    }

    /**
     * Login Acknowledgement
     */
    public function login(LoginRequest $request)
    {
        $data = $request->validated();

        $credentials = $request->only('username', 'password');

        if (Auth::attempt($credentials)) {
            // Authentification réussie, créer une session
            $request->session()->regenerate();

            return redirect()->route('admin.dashboard');
        }
        // Authentification échouée, renvoyer à la vue de connexion avec un message d'erreur
        return back()->withInput()->withErrors([
            'username' => 'Invalid credentials.',
        ]);
    }

    /**
     * Logout
     */
    public function logout(Request $request)
    {
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return redirect('login');
    }

    /**
     * Logout
     */
    public function reset_password(Request $request)
    {
        return view('auth.password.reset');
    }
}