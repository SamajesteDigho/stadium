<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('stadiums', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('location')->nullable();
            $table->integer('capacity')->default(0);
            $table->string('photo')->nullable();
            $table->enum('section', ['NORTH', 'SOUTH', 'EAST', 'WEST', 'VIP', 'VVIP', 'PRESIDENTIAL'])->default('EAST');
            $table->enum('status', ['PENDING', 'ACTIVE', 'SUSPENDED', 'DELETED']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('stadiums');
    }
};