<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\EventController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\StadiumController;
use App\Http\Controllers\TicketController;
use App\Http\Controllers\TeamController;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\ClientController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('index', [ClientController::class, 'show'])->name('home');

Route::get('re', function () {
    return view('auth.re');
});

Route::group(['prefix' => ''], function () {
    Route::get('register', [AuthController::class, 'register_page'])->name('register');
    Route::post('register', [AuthController::class, 'register'])->name('register');
    Route::get('login', [AuthController::class, 'login_page'])->name('login');
    Route::post('login', [AuthController::class, 'login'])->name('login');
    Route::post('logout', [AuthController::class, 'logout'])->name('logout');
    Route::get('reset_password', [AuthController::class, 'reset_password'])->name('reset_password');
});


Route::group(['prefix' => 'admin'], function () {
    Route::get('', [DashboardController::class, 'dashboard'])->name('admin.dashboard');
    Route::group(['prefix' => 'users'], function () {
        Route::get('', [UserController::class, 'index'])->name('admin.users.list');
        Route::get('create', [userController::class, 'create'])->name('admin.users.create');
        Route::post('store', [UserController::class, 'store'])->name('admin.users.store');
        Route::get('edit', [UserController::class, 'edit'])->name('admin.users.edit');
        Route::post('update', [UserController::class, 'update'])->name('admin.users.update');
        Route::get('delete', [UserController::class, 'delete'])->name('admin.users.delete');
    });
    Route::group(['prefix' => 'events'], function () {
        Route::get('', [EventController::class, 'index'])->name('admin.events.list');
        Route::get('create', [EventController::class, 'create'])->name('admin.events.create');
        Route::post('store', [EventController::class, 'store'])->name('admin.events.store');
        Route::get('edit', [EventController::class, 'edit'])->name('admin.events.edit');
        Route::post('update', [EventController::class, 'update'])->name('admin.events.update');
        Route::get('delete', [EventController::class, 'delete'])->name('admin.events.delete');
    });
    Route::group(['prefix' => 'stadiums'], function () {
        Route::get('', [StadiumController::class, 'index'])->name('admin.stadiums.list');
        Route::get('create', [StadiumController::class, 'create'])->name('admin.stadiums.create');
        Route::post('store', [StadiumController::class, 'store'])->name('admin.stadiums.store');
        Route::get('edit', [StadiumController::class, 'edit'])->name('admin.stadiums.edit');
        Route::post('update', [StadiumController::class, 'update'])->name('admin.stadiums.update');
        Route::get('delete', [StadiumController::class, 'delete'])->name('admin.stadiums.delete');
    });
    Route::group(['prefix' => 'tickets'], function () {
        Route::get('', [TicketController::class, 'index'])->name('admin.tickets.list');
        Route::get('create', [TicketController::class, 'create'])->name('admin.tickets.create');
        Route::post('store', [TicketController::class, 'store'])->name('admin.tickets.store');
        Route::get('edit', [TicketController::class, 'edit'])->name('admin.tickets.edit');
        Route::post('update', [TicketController::class, 'update'])->name('admin.tickets.update');
        Route::get('delete', [TicketController::class, 'delete'])->name('admin.tickets.delete');
    });
    Route::group(['prefix' => 'teams'], function () {
        Route::get('', [TeamController::class, 'index'])->name('admin.teams.list');
        Route::get('create', [TeamController::class, 'create'])->name('admin.teams.create');
        Route::post('store', [TeamController::class, 'store'])->name('admin.teams.store');
        Route::get('edit', [TeamController::class, 'edit'])->name('admin.teams.edit');
        Route::post('update', [TeamController::class, 'update'])->name('admin.teams.update');
        Route::get('delete', [TeamController::class, 'delete'])->name('admin.teams.delete');
    });
    Route::group(['prefix' => 'payments'], function () {
        Route::get('', [PaymentController::class, 'index'])->name('admin.payments.list');
        Route::get('create', [PaymentController::class, 'create'])->name('admin.payments.create');
        Route::post('store', [PaymentController::class, 'store'])->name('admin.payments.store');
        Route::get('edit', [PaymentController::class, 'edit'])->name('admin.payments.edit');
        Route::post('update', [PaymentController::class, 'update'])->name('admin.payments.update');
        Route::get('delete', [PaymentController::class, 'delete'])->name('admin.payments.delete');
    });
});
