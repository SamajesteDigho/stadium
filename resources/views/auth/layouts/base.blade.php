<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from dlab-html.vercel.app/index-2.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 20 Feb 2023 08:34:40 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>SMS{{ (isset($page)) ? ' - '.$page : '' }}</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/admin/calender.css') }}">
    <link href="{{ asset('assets/css/admin/style.css') }}" rel="stylesheet">

</head>

<body>


    <!--**********************************
        Main wrapper start
    ***********************************-->
    <div id="main-wrapper">

        @yield('content')

    </div>





    <!--**********************************
        Scripts
    ***********************************-->
    <!-- Required vendors -->
    <script src="{{ asset('assets/js/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/js/boostrap/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/js/admin/jquery.slimscroll.min.js') }}"></script>
    <!-- Here is navigation script -->
    <script src="{{ asset('assets/js/admin/quixnav.min.js') }}"></script>
    <script src="{{ asset('assets/js/admin/quixnav-init.js') }}"></script>
    <script src="{{ asset('assets/js/admin/custom.min.js') }}"></script>
    <!--removeIf(production)-->
    <!-- Demo scripts -->
    <script src="{{ asset('assets/js/admin/styleSwitcher.js') }}"></script>
    <!--endRemoveIf(production)-->

    <!-- Daterange picker library -->
    <script src="{{ asset('assets/js/admin/circle-progress.min.js') }}"></script>
    
    <!-- Vectormap -->
    <script src="{{ asset('assets/js/admin/vmap/jquery.vmap.min.js') }}"></script>
    <script src="{{ asset('assets/js/admin/vmap/jquery.vmap.world.js') }}"></script>


    <!-- calender -->
    <script src="{{ asset('assets/js/admin/calender.min.js') }}"></script>
    <script src="{{ asset('assets/js/admin/calender-init.js') }}"></script>

    <!-- Chart Morris plugin files -->
    <script src="{{ asset('assets/js/admin/raphael.min.js') }}"></script>
    <script src="{{ asset('assets/js/admin/morris.min.js') }}"></script>

    
    <script src="{{ asset('assets/js/admin/Chart.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/js/admin/dashboard-1.js') }}"></script>

</body>
</html>