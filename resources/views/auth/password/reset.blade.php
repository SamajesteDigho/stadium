@extends('auth.layouts.base')

@section('content')
<div class="container mt-5">
    <div class="card bg-light">
        <div class="card-header">
            Reset Password
        </div>
        <div class="card-body">
            Reset your password here.
            <p class="text-red">Still in development</span>
        </div>
    </div>
</div>
@endsection