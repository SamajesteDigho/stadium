<div class="quixnav">
    <div class="quixnav-scroll">
        <ul class="metismenu" id="menu">
            <li class="nav-label">Navigation</li>
            <li><a class="" href="{{ route('admin.dashboard') }}" aria-expanded="false"><i class="mdi mdi-home"></i><span class="nav-text">Dashboard</span></a></li>

            <li class="nav-label">ACTIONS</li>

            <li><a class="has-arrow" href="javascript:void()" aria-expanded="false"><i class="mdi mdi-people"></i><span class="nav-text">Users</span></a>
                <ul aria-expanded="false">
                    <li><a href="{{ route('admin.users.list') }}">List</a></li>
                </ul>
            </li>

            <li><a class="has-arrow" href="javascript:void()" aria-expanded="false"><i class="mdi mdi-eventbrite"></i><span class="nav-text">Events</span></a>
                <ul aria-expanded="false">
                    <li><a href="{{ route('admin.events.list') }}">List</a></li>
                    <li><a href="{{ route('admin.events.create') }}">Create</a></li>
                </ul>
            </li>

            <li><a class="has-arrow" href="javascript:void()" aria-expanded="false"><i class="mdi mdi-stadium"></i><span class="nav-text">Stadiums</span></a>
                <ul aria-expanded="false">
                    <li><a href="{{ route('admin.stadiums.list') }}">List</a></li>
                    <li><a href="{{ route('admin.stadiums.create') }}">Create</a></li>
                </ul>
            </li>

            <li><a class="has-arrow" href="javascript:void()" aria-expanded="false"><i class="mdi mdi-ticket"></i><span class="nav-text">Tickets</span></a>
                <ul aria-expanded="false">
                    <li><a href="{{ route('admin.tickets.list') }}">List</a></li>
                    <li><a href="{{ route('admin.tickets.create') }}">Create</a></li>
                </ul>
            </li>



            <li><a class="has-arrow" href="javascript:void()" aria-expanded="false"><i class="mdi mdi-"></i><span class="nav-text">Payments</span></a>
                <ul aria-expanded="false">
                    <li><a href="{{ route('admin.payments.list') }}">List</a></li>
                    <li><a href="{{ route('admin.payments.create') }}">Create</a></li>
                </ul>
            </li>

            <li><a class="has-arrow" href="javascript:void()" aria-expanded="false"><i class="mdi mdi-"></i><span class="nav-text">Teams</span></a>
                <ul aria-expanded="false">
                    <li><a href="{{ route('admin.teams.list') }}">List</a></li>
                    <li><a href="{{ route('admin.teams.create') }}">Create</a></li>
                </ul>
            </li>
        </ul>
    </div>
</div>
        