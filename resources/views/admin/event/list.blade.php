@extends('admin.layouts.base')

@section('content')

<div class="col-lg-12">
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Events</h4>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-responsive-sm">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Date</th>
                            <th>Team 1</th>
                            <th>Team 2</th>
                            <th>Stadium</th>
                            <th>Details</th>
                            <th>Created Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($events as $event)
                        <tr>
                            <th> {{ $event->id }} </th>
                            <td> {{ $event->name }} </td>
                            <td> {{ $event->time }} </td>
                            <td> {{ $event->team1->name }} </td>
                            <td> {{ $event->team2->name }} </td>
                            <td> {{ $event->stadium->name }} </td>
                            <td> {{ $event->details }} </td>
                            <td><span class="badge badge-primary">{{ $event->status }}</span></td>
                            <td> {{ $event->created_at }} </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


@endsection