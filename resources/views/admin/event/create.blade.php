@extends('admin.layouts.base')

@section('content')
<div class="row justify-content-center">
    <div class="col-lg-6">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">Create Event</h4>
            </div>
            <div class="card-body">
                <div class="create-event-form">
                    <form action="{{ route('admin.events.store') }}" method="POST">
                        @csrf
                        <h5 class="mb-3">General Info</h5>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label for="name">Name</label>
                                <input type="name" class="form-control @error('name') is-invalid @enderror" id="name" placeholder="" name="name">
                                @error('name')
                                    <span class="invalid-feedback">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label for="date">Date & Time</label>
                                <input type="datetime-local" class="form-control @error('date') is-invalid @enderror " id="date" placeholder="" name="date">
                                @error('date')
                                        <span class="invalid-feedback">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                @enderror
                            </div>
                        </div>
                        
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="team1">Team 1</label>
                                <select id="team1" name="team1" class="form-control @error('team1') is-invalid @enderror">
                                    <option>Choose</option>
                                    @foreach($teams as $team)
                                    <option value="{{ $team->id }}">{{ $team->name }}</option>
                                    @endforeach
                                </select>
                                @error('team1')
                                        <span class="invalid-feedback">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                @enderror
                            </div>
                            <div class="form-group col-md-6">
                                <label for="team2">Team 2 </label>
                                <select id="team2" name="team2" class="form-control @error('team2') is-invalid @enderror">
                                    <option>Choose</option>
                                    @foreach($teams as $team)
                                    <option value="{{ $team->id }}">{{ $team->name }}</option>
                                    @endforeach
                                </select>
                                @error('team2')
                                        <span class="invalid-feedback">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                @enderror
                            </div>
                            <div class="form-group col-md-12">
                                <label for="stadium">Stadium</label>
                                <select id="stadium" name="stadium" class="form-control @error('stadium') is-invalid @enderror">
                                    <option>Choose</option>
                                    @foreach($stadiums as $stadium)
                                    <option value="{{ $stadium->id }}">{{ $stadium->name }}</option>
                                    @endforeach
                                </select>
                                @error('stadium')
                                    <span class="invalid-feedback">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="details">Details</label>
                            <div class="form-group">
                                <textarea class="form-control" rows="4" id="details" name="details"></textarea>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
    </div>
@endsection