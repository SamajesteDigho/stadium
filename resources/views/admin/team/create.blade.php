@extends('admin.layouts.base')

@section('content')

<div class="row justify-content-center">
        <div class="col-lg-6">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Create Team</h4>
                </div>
                <div class="card-body">
                    <div class="create-event-form">
                        <form action="{{ route('admin.teams.store') }}" method="POST" >
                            @csrf
                            <h5 class="mb-3">General Info</h5>
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label for="name">Name</label>
                                    <input type="name" class="form-control @error('name') is-invalid @enderror" id="name" placeholder="" name="name">
                                    @error('name')
                                        <span class="invalid-feedback">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label for="short_name">Short Name</label>
                                    <input type="name" class="form-control @error('short_name') is-invalid @enderror" id="short_name" placeholder="" name="short_name">
                                    @error('short_name')
                                        <span class="invalid-feedback">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label for="manager">Manager</label>
                                    <input type="text" class="form-control @error('manager') is-invalid @enderror" id="manager" placeholder="" name="manager">
                                    @error('manager')
                                        <span class="invalid-feedback">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label for="flag">Flag</label>
                                    <input type="file" class="form-control @error('flag') is-invalid @enderror" id="flag" placeholder="" name="flag">
                                    @error('flag')
                                        <span class="invalid-feedback">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection