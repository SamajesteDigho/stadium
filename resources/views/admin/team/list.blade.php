@extends('admin.layouts.base')

@section('content')

<div class="col-lg-12">
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Teams</h4>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-responsive-sm">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Short Name</th>
                            <th>Manager</th>
                            <th>Status</th>
                            <th>Created Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($teams as $team)
                        <tr>
                            <th> {{ $team->id }} </th>
                            <td> {{ $team->name }} </td>
                            <td> {{ $team->short_name }} </td>
                            <td> {{ $team->manager }} </td>
                            <td><span class="badge badge-primary">{{ $team->status }}</span></td>
                            <td> {{ $team->created_at }} </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


@endsection