@extends('admin.layouts.base')

@section('content')

<div class="col-lg-12">
    <div class="card">
        <div class="card-header">
            <h4 class="card-title">Stadiums</h4>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-responsive-sm">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Locatioin</th>
                            <th>Capacity</th>
                            <th>Status</th>
                            <th>Created Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($stadiums as $stadium)
                        <tr>
                            <th> {{ $stadium->id }} </th>
                            <td> {{ $stadium->name }} </td>
                            <td> {{ $stadium->location }} </td>
                            <td> {{ $stadium->capacity }} </td>
                            <td><span class="badge badge-primary">{{ $stadium->status }}</span></td>
                            <td> {{ $stadium->created_at }} </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


@endsection