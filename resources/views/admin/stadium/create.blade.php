@extends('admin.layouts.base')

@section('content')

<div class="row justify-content-center">
                    <div class="col-lg-6">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Create Stadium</h4>
                            </div>
                            <div class="card-body">
                                <div class="create-event-form">
                                    <form action="{{ route('admin.stadiums.store') }}" method="POST">
                                      @csrf
                                        <h5 class="mb-3">General Info</h5>
                                        <div class="form-row">
                                            <div class="form-group col-md-12">
                                                <label for="name">Name</label>
                                                <input type="name" class="form-control @error('name') is-invalid @enderror" id="name" placeholder="" name="name">
                                                @error('name')
                                                    <span class="invalid-feedback">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col-md-12">
                                                <label for="location">Lacation</label>
                                                <input type="text" class="form-control @error('location') is-invalid @enderror" id="location" placeholder="" name="location">
                                                @error('location')
                                                    <span class="invalid-feedback">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col-md-12">
                                                <label for="capacity">Capacity</label>
                                                <input type="number" class="form-control @error('capacity') is-invalid @enderror" id="capacity" placeholder="" name="capacity">
                                                @error('capacity')
                                                    <span class="invalid-feedback">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col-md-12">
                                                <label for="photo">Photo</label>
                                                <input type="file" class="form-control @error('photo') is-invalid @enderror" id="photo" placeholder="" name="photo">
                                                @error('photo')
                                                    <span class="invalid-feedback">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

@endsection