@extends('admin.layouts.base')

@section('content')
        <div class="row page-titles mx-0">
            <div class="col-sm-6 p-md-0">
                <h5 class="mb-0">Hi, <b>{{ Auth()->user()->firstname }}</b>. <small>Welcome</small>!</h5>
            </div>
            <div class="col-sm-6 p-md-0 justify-content-sm-end mt-2 mt-sm-0 d-flex">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                    <li class="breadcrumb-item active"><a href="javascript:void(0)">Dashboard</a></li>
                </ol>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-4 col-xxl-4 col-lg-6 col-md-4">
                <div class="card widget-stat">
                    <div class="chart-wrapper bg-primary pt-5">
                        <canvas id="chart_widget_1"></canvas> 
                    </div>
                    <div class="card-body">
                        <div class="d-flex justify-content-between">
                            <div>
                                <h4 class="text-dark"> Online Sales</h4>
                                <span class="text-muted">Sales Of This Month | 45% <i class="mdi mdi-arrow-up-bold text-success"></i></span>
                            </div>
                            <h3 class="text-dark">$260</h3>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-xxl-4 col-lg-6 col-md-4">
                <div class="card widget-stat">
                    <div class="chart-wrapper bg-success pt-5">
                        <canvas id="chart_widget_2"></canvas>
                    </div>
                    <div class="card-body">
                        <div class="d-flex justify-content-between">
                            <div>
                                <h4 class="text-dark">Total Earning</h4>
                                <span class="text-muted">Earning Of This Month | 4% <i class="mdi mdi-arrow-down-bold text-danger"></i></span>
                            </div>
                            <h3 class="text-dark">$950</h3>
                        </div>
                    </div>
                </div>
            </div>
            <!-- <div class="col-xl-3 col-xxl-3 col-lg-6 col-md-6">
                <div class="card widget-stat">
                    <div class="card-body">
                        <h3 class="text-dark"> 260</h3>
                        <p>Total Earning</p>
                        <div class="chart-wrapper">
                            <canvas id="chart_widget_3"></canvas>
                        </div>
                    </div>
                </div>
            </div> -->
            <div class="col-xl-4 col-xxl-4 col-lg-6 col-md-4">
                <div class="card widget-stat">
                    <div class="chart-wrapper bg-info pt-5">
                        <canvas id="chart_widget_04"></canvas>
                    </div>
                    <div class="card-body">
                        <div class="d-flex justify-content-between">
                            <div>
                                <h4 class="text-dark">Total Profit</h4>
                                <span class="text-muted">Profit Of This Month | 4% <i class="mdi mdi-arrow-down-bold text-danger"></i></span>
                            </div>
                            <h3 class="text-dark">$2050</h3>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-3 col-xxl-6 col-lg-12">
                <div class="card">
                    <div class="card-header d-block">
                        <h4 class="card-title">Event Goals</h4>
                    </div>
                    <div class="card-body event-goals">
                        <div class="row">
                            <div class="col-xl-12 col-xxl-12">
                                <p class="mb-2">Wordpress Theme Development
                                    <span class="float-right font-weight-bold text-dark">85%</span>
                                </p>
                                <div class="progress mb-4">
                                    <div class="progress-bar bg-primary progress-animated" style="width: 85%; height:6px;" role="progressbar">
                                    </div>
                                </div>
                                
                            </div>
                            <div class="col-xl-12 col-xxl-12">
                                <p class="mb-2">UI Design
                                    <span class="float-right font-weight-bold text-dark">65%</span>
                                </p>
                                <div class="progress mb-4">
                                    <div class="progress-bar bg-success progress-animated" style="width: 65%; height:6px;" role="progressbar">
                                    </div>
                                </div>
                                
                            </div>
                            <!-- <div class="col-xl-12 col-xxl-12">
                                <p class="mb-2">Front end Development
                                    <span class="float-right font-weight-bold">50%</span>
                                </p>
                                <div class="progress mb-4">
                                    <div class="progress-bar bg-danger progress-animated" style="width: 50%; height:6px;" role="progressbar">
                                    </div>
                                </div>
                                
                            </div> -->
                            <div class="col-xl-12 col-xxl-12">
                                <p class="mb-2">Digital Marketing
                                    <span class="float-right font-weight-bold">65%</span>
                                </p>
                                <div class="progress mb-3">
                                    <div class="progress-bar bg-dark progress-animated" style="width: 65%; height:6px;" role="progressbar">
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="col-xl-3 col-xxl-6 col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Bloomreach Connect</h4>
                    </div>
                    <div class="card-body">
                        <div class="event-attend">
                            <div class="attend-member">
                                <div id="attend_event_1"></div>
                                <h5><i class="mdi mdi-account"></i> 960</h5>
                                <h6 class="text-dark mt-2 mb-0">Attend</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-xxl-6 col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Tech Inclusion</h4>
                    </div>
                    <div class="card-body">
                        <div class="event-attend">
                            <div class="attend-member">
                                <div id="attend_event_2"></div>
                                <h5><i class="mdi mdi-account"></i> 1250</h5>
                                <h6 class="text-dark mt-2 mb-0">Attended</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-xxl-6 col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">
                                Presto Summit</h4>
                    </div>
                    <div class="card-body">
                        <div class="event-attend">
                            <div class="attend-member">
                                <div id="attend_event_3"></div>
                                <h5><i class="mdi mdi-account"></i> 100</h5>
                                <h6 class="text-dark mt-2 mb-0">Ongoing</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xl-8 col-xxl-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Sales Overview</h4>
                    </div>
                    <div class="card-body">
                        <div class="row mb-3">
                            <div class="col-4 col-4">
                                <p class="mb-2">Ticket 1</p>
                                <h4 class="text-dark">$5500.00</h4>
                            </div>
                            <div class="col-4 col-4">
                                <p class="mb-2">Ticket 2</p>
                                <h4 class="text-dark">$6550.00</h4>
                            </div>
                            <div class="col-4 col-4">
                                <p class="mb-2">Ticket 3</p>
                                <h4 class="text-dark">$7540.00</h4>
                            </div>
                        </div>
                        <div id="sales_overview" class="morris_chart_height"></div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-xxl-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Upcoming Events</h4>
                    </div>
                    <div class="card-body">
                        <div id="timeline-activity">
                            <ul class="timeline mb-0">
                                <li>
                                    <div class="timeline-badge bg-primary"></div>
                                    <a href="#" class="timeline-panel">
                                        <span>January 22, Monday | 11:00 AM</span>
                                        <h5 class="mt-2">Digital Marketing</h5>
                                        <div class="label label-danger">Sold out</div>
                                    </a>
                                </li>
                                <li>
                                    <div class="timeline-badge bg-success"></div>
                                    <a href="#" class="timeline-panel">
                                        <span>January 22, Monday | 11:00 AM</span>
                                        <h5 class="mt-2">5 Orders Delivered</h5>
                                        <div class="label label-warning">Pending</div>
                                    </a>
                                </li>
                                <li>
                                    <div class="timeline-badge bg-warning"></div>
                                    <a href="#" class="timeline-panel">
                                        <span>January 22, Monday | 11:00 AM</span>
                                        <h5 class="mt-2">3 New Tickets</h5>
                                        <div class="label label-danger">Sold out</div>
                                    </a>
                                </li>
                                <li>
                                    <div class="timeline-badge bg-primary"></div>
                                    <a href="#" class="timeline-panel">
                                        <span>January 22, Monday | 11:00 AM</span>
                                        <h5 class="mt-2">8 New Reviews</h5>
                                        <div class="label label-success">Free</div>
                                    </a>
                                </li>
                                <li>
                                    <div class="timeline-badge bg-dark"></div>
                                    <a href="#" class="timeline-panel">
                                        <span>January 22, Monday | 11:00 AM</span>
                                        <h5 class="mt-2">50 New Facebook likes</h5>
                                        <div class="label label-secondary">Pending</div>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection