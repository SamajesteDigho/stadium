@extends('user.layouts.base')
@section('content')
    @extends('user.layouts.banner')

    <section>
        <div class="container">
            <div class="row my-3">
                @foreach ($events as $eve)
                    <div class="col-4">
                        <div class="card">
                            <div class="card-header">
                                <div class="row justify-content-center">
                                    <div class="col-4 text-center">
                                        <img src="{{ asset('assets/images/teams/' . $eve->team1->name . '.png') }}"
                                            style="width: 120px; height: 120px" class="img-fluid" alt="image">
                                        <strong>{{ $eve->team1->name }}</strong>
                                    </div>
                                    <div class="col-3">
                                        <div class="row justify-content-center">
                                            <img src="{{ asset('assets/images/teams/caf.png') }}" width="50"
                                                height="50" class="img-fluid" alt="image">
                                        </div>
                                        <div class="row justify-content-center">
                                            <img src="{{ asset('assets/images/teams/trophy.png') }}" width="50"
                                                height="50" class="img-fluid" alt="image">
                                        </div>
                                    </div>
                                    <div class="col-4 text-center">
                                        <img src="{{ asset('assets/images/teams/' . $eve->team2->name . '.png') }}"
                                            style="width: 120px; height: 120px" class="img-fluid" alt="image">
                                        <strong>{{ $eve->team2->name }}</strong>
                                    </div>
                                </div>
                            </div>
                            <div class="js-grid-item-body event_body__BfZIC">
                                <div class="event_calendar__2x4Hv">
                                    <span class="event_month__S8D_o color-primary">WED</span>
                                    <span class="event_date__2Z7TH">{{ $eve->created_at->format('d') }}</span>
                                </div>

                                <div class="event_content__2fB-4">
                                    <h2 class="event_title__3C2PA"><a href="/event-detail">{{ $eve->name }}</a></h2>

                                    <ul class="event_meta__CFFPg list-none">
                                        <li class="event_metaList__1bEBH text-ellipsis">
                                            <span>
                                                <i class="fa fa-calendar"></i>
                                                {{ $eve->created_at->format('M d y') }}
                                            </span>
                                        </li>
                                        <li class="event_metaList__1bEBH text-ellipsis">
                                            <span>
                                                <i class="fa fa-clock-o"></i>
                                                {{ $eve->created_at->format('H:m') }}
                                            </span>
                                        </li>

                                        <li class="event_metaList__1bEBH text-ellipsis">
                                            <span>
                                                <a href="#" target="_blank">
                                                    <span><i class="fa fa-map-marker"></i>{{ $eve->stadium->name }}</span>
                                                </a>
                                            </span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>

    <!-- About ESTADIO-->
    <section id="about" class="about-section section-padding">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <h2>Why Choose ESTADIO EVENTS</h2>
                    <p>Because our digital story started with our dreams integrated manually.</p>
                </div>
                <div class="service-box col-sm-3">
                    <div class="gt-icon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    <div class="gt-title home-title">
                        <h2>MULTIPLE EVENTS</h2>
                        <p>ESTADIO proides you with all the events of your choice. You can also host your events on any
                            of our stadiums
                        <p>
                    </div>
                </div>
                <div class="service-box col-sm-3">
                    <div class="gt-icon">
                        <i class="fa fa-adjust"></i>
                    </div>
                    <div class="gt-title home-title">
                        <h2>EVENT MANAGEMENT</h2>
                        <p>ESTADIO manages all the events at its disposals including ticket selling, organization and
                            ensuring customers have the best experience</p>
                    </div>
                </div>
                <div class="service-box col-sm-3">
                    <div class="gt-icon">
                        <i class="fa fa-credit-card"></i>
                    </div>
                    <div class="gt-title home-title">
                        <h2>CREDIT CARD PAYMENT</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas et ultrices massa, sed
                            porta dui.</p>
                    </div>
                </div>
                <div class="service-box col-sm-3">
                    <div class="gt-icon">
                        <i class="fa fa-street-view"></i>
                    </div>
                    <div class="gt-title home-title">
                        <h2>GOOGLE STREET VIEW</h2>
                        <p>ESTADIO provides its users with the street view of all the event venues to ensure they don't
                            find difficulties in location</p>
                    </div>
                </div>


            </div>
        </div>
    </section>
    <!-- /About -->


    </section>
    <!-- /Event Category -->
    <!-- Testimonial-Style-1 -->
    <section id="testimonial" class="section-padding gray-bg">
        <div class="container">
            <div class="row">
                Heading
                <div class="col-md-12">
                    <div class="heading-sec">
                        <div class="section-header text-center">
                            <h2>What Our Client Says</h2>
                        </div>
                    </div>
                </div>

                <div id="testimonial_slider" class="owl-carousel">
                    <div class="item">
                        <div class="testimonial_head">
                            <div class="testimonial_img"> <img src="assets/images/profile/testimonial_img1.jpg"
                                    class="img-fluid" alt="image"> </div>
                            <h5>Samantha Doe</h5>
                            <small>CEO of Lorem lpsum</small>
                        </div>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
                            been the industry's standard dummy text ever since the 1500s, when an unknown printer took a
                            galley of type and scrambled it to make a type specimen book. It has survived not only five
                            centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
                        </p>
                    </div>
                    <div class="item">
                        <div class="testimonial_head">
                            <div class="testimonial_img"> <img src="assets/images/profile/testimonial_img2.jpg"
                                    class="img-fluid" alt="image"> </div>
                            <h5>Samantha Doe</h5>
                            <small>CEO of Lorem lpsum</small>
                        </div>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
                            been the industry's standard dummy text ever since the 1500s, when an unknown printer took a
                            galley of type and scrambled it to make a type specimen book. It has survived not only five
                            centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
                        </p>
                    </div>
                    <div class="item">
                        <div class="testimonial_head">
                            <div class="testimonial_img"> <img src="assets/images/profile/testimonial_img1.jpg"
                                    class="img-fluid" alt="image"> </div>
                            <h5>Samantha Doe</h5>
                            <small>CEO of Lorem lpsum</small>
                        </div>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
                            been the industry's standard dummy text ever since the 1500s, when an unknown printer took a
                            galley of type and scrambled it to make a type specimen book. It has survived not only five
                            centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
                        </p>
                    </div>
                    <div class="item">
                        <div class="testimonial_head">
                            <div class="testimonial_img"> <img src="assets/images/profile/testimonial_img2.jpg"
                                    class="img-fluid" alt="image"> </div>
                            <h5>Samantha Doe</h5>
                            <small>CEO of Lorem lpsum</small>
                        </div>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
                            been the industry's standard dummy text ever since the 1500s, when an unknown printer took a
                            galley of type and scrambled it to make a type specimen book. It has survived not only five
                            centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
