<!DOCTYPE HTML>
<html lang="en">



<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <title>SMS</title>

    <!--Bootstrap -->
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap/bootstrap.min.css') }}">
    <!--Custome Style -->
    <link rel="stylesheet" href="{{ asset('assets/css/user/style.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/css/user/custom.css') }}" type="text/css">
    <!--OWL Carousel slider-->
    <link rel="stylesheet" href="{{ asset('assets/css/user/owl.carousel.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ asset('assets/css/user/owl.transitions.css') }}" type="text/css">
    <!-- SWITCHER -->
    <!--FontAwesome Font Style -->
    <script src="{{ asset('assets/js/user/c83d468820.js') }}" crossorigin="anonymous"></script>

    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144"
        href="assets/images/favicon-icon/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114"
        href="assets/images/favicon-icon/apple-touch-icon-114-precomposed.html">
    <link rel="apple-touch-icon-precomposed" sizes="72x72"
        href="assets/images/favicon-icon/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="assets/images/favicon-icon/apple-touch-icon-57-precomposed.png">
    <link rel="shortcut icon" href="assets/images/favicon-icon/favicon.png">
    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>

<body>
    @include('user.layouts.header')

    @yield('content')

    @include('user.layouts.footer')

    <!-- Scripts -->
    <script src="{{ asset('assets/js/jquery.min.js') }}"></script>
    <!-- <script src="http://maps.google.com/maps/api/js?sensor=true"></script>
<script src="assets/js/gmaps.min.js"></script>  -->
    <script src="{{ asset('assets/js/user/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('assets/js/bootstrap/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/js/user/interface.js') }}"></script>
    <script src="{{ asset('assets/js/user/owl.carousel.min.js') }}"></script>
    <!--Switcher-->
    <script src="{{ asset('assets/js/user/switcher.js') }}"></script>


    <!-- Countdown-->
    <script src="{{ asset('assets/js/user/jquery.countdown.min.js') }}"></script>
</body>

<!-- Mirrored from themes.webmasterdriver.net/BeEvent/demo-3/with-paypal/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 17 Jan 2023 10:56:46 GMT -->

</html>
