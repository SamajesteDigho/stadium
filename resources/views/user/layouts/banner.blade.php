
<section id="banner" class="banner-section">
		<div id="slideshow" class="slideshow">
			<div class="slides" style="background-image:url(assets/images/stadiums/banner.jpg)">
				<div class="banner-fixed">
					<div class="container">
						<div class="banner-content">
							<div class="content">
								<div class="banner-tagline text-center">
									<h1 style="color:green">OLEMBE STADIUM</h1>
									<ul class="gt-information">
										<li><i class="fa fa-map-marker"></i><span style="color: green;">YAOUNDE, CAMEROON</span></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="slides" style="background-image:url(assets//images/stadiums/olem.PNG)">
				<div class="banner-fixed">
					<div class="container">
						<div class="banner-content">
							<div class="content">
								<div class="banner-tagline text-center">

									<h1>OLEMBE STADIUM</h1>
									<ul class="gt-information">
										<li><i class="fa fa-map-marker"></i><span>YAOUNDE</span></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<!-- Banners Timer -->
		<!--<div class="container countdown-tab">
			<div class="countdown">
				<div class="counter-sec">
					<div class="countdown-counter half-width text-center">
						<div class="timer">
							<div class="countdown styled"></div>
						</div>
					</div>
					<div class="countdown-btn half-width">
						<a href="#" class="btn btn-lg btn-danger" data-toggle="modal"
							data-target="#registration_form">Purchase Ticket</a>
					</div>
				</div>
			</div>
		</div>-->
		<!-- Banners Timer -->
		<!--Registration Form-->
	<div id="registration_form" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content padding_4x4_40 text-center">
				<div class="modal-header">
					<h3>Reserve Your Seat</h3>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
							aria-hidden="true">&times;</span></button>
				</div>
				<form id="paypal-form"
					action="http://themes.webmasterdriver.net/BeEvent/demo-3/with-paypal/paypal/index.php" method="get">

					<div class="form-group">
						<input class="form-control" name="payer_fname" type="text" placeholder="Full Name" required>
					</div>

					<div class="form-group">
						<input class="form-control" name="payer_email" type="email" placeholder="Email Address"
							required>
					</div>

					<div class="form-group">
						<input class="form-control" name="phone" type="text" placeholder="Phone Number" required>
					</div>

					<div class="form-group">
						<select class="form-control" name="product_quantity" required>
							<option value="">Number of Seats</option>
							<option value="1">1 Seat</option>
							<option value="2">2 Seats</option>
							<option value="3">3 Seats</option>
							<option value="4">4 Seats</option>
							<option value="5">5 Seats</option>
						</select>
					</div>

					<div class="form-group">
						<select class="form-control" name="product_name" required>
							<option value="">Presidential Loge</option>
							<option value="FrontSeat">VVIP TRIBUNE</option>
							<option value="MiddleSeat">VIP TRIBUNE</option>
							<option value="BackSeat">EAST TRIBUNE</option>
							<option value="VIP">SOUTH TRIBUNE</option>
							<option value="VIP">NORTH TRIBUNE</option>

						</select>
					</div>

					<div class="form-group">
						<input class="btn" name="submit" type="submit" value="Buy Tickets!">
					</div>
				</form>
			</div>
		</div>
	</div>
	<!--/Registration Form-->

	<!-- Registration-success-message -->
	<div id="success" class="modal modal-message fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header"> <span class="close" data-bs-dismiss="modal" aria-label="Close"><span
							aria-hidden="true">&times;</span></span>
					<h2 class="modal-title">Thank You!</h2>
					<p class="modal-subtitle">Your message is successfully sent...</p>
				</div>
			</div>
		</div>
	</div>
	<!--/Registration-success-message	-->

	<!-- Registration-error -->
	<div id="error" class="modal modal-message fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header"> <span class="close" data-bs-dismiss="modal" aria-label="Close"><span
							aria-hidden="true">&times;</span></span>
					<h2 class="modal-title">Sorry...</h2>
					<p class="modal-subtitle"> Something went wrong </p>
				</div>
			</div>
		</div>
	</div>
	<!--/Registration-error--> 

</section>

	
