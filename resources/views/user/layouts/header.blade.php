
<!--Header-->
<header id="header" class="bg-dark">
		<div class="container">
			<div class="row">
				<div class="col-sm-3 col-md-2">
					<div class="logo">
						<h3><a href="/index"><span>S</span>M<span>S</span></a></h3>
					</div>
				</div>
				<div class="col-sm-9 col-md-10">
					<!-- Navigation -->
					<nav class="navbar navbar-expand-lg">

						<button class="navbar-toggler" type="button" data-bs-toggle="collapse"
						data-bs-target="#navigation" aria-controls="navbarSupportedContent" aria-expanded="false"
							aria-label="Toggle navigation">
							<i class="fa fa-bars" aria-hidden="true"></i>
						</button>

						<div class="collapse navbar-collapse" id="navigation">
							<ul class="nav navbar-nav ml-auto">
								<li class="active dropdown"><a href="/index">Home</a>

								<li class="dropdown"><a class="dropdown-toggle" href="event.html" role="button"
										data-bs-toggle="dropdown">All Event</a>
									<ul class="dropdown-menu">
										<li><a href="/upcoming">Upcoming Event</a></li>
										<li><a href="/expired">Expired Event</a></li>
										<li><a href="/event-detail">Event Detail Page</a></li>
									</ul>
								</li>
								<li class="dropdown"><a class="dropdown-toggle" href="#" role="button"
										data-bs-toggle="dropdown">Pages</a>
									<ul class="dropdown-menu">
										<li><a href="/about-us">About Us</a></li>
										<li><a href="/contact">Contact Us</a></li>
									</ul>
								</li>
								<li class="dropdown">
									<a class="dropdown-toggle" href="#" id="navbarDropdown" role="button"
										data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Elements
									</a>
									<ul class="dropdown-menu">
										<li><a href="/gallery">Gallery</a></li>
										<li><a href="/stadiums">Other Stadiums</a></li>
									</ul>
								</li>
							</ul>
						</div>
					</nav>
					<!-- Navigation end -->
				</div>
			</div>
		</div>
	</header>

