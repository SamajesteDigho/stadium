<!--Footer-->
<footer class="secondary-bg footer-td">
		<div class="container">
			<div class="row">
				<div class="col-md-3 popular-link">
					<h5>Popular Links</h5>
					<div class="menu-footer-menu-container">
						<ul id="menu-footer-menu" class="menu">
							<li id="menu-item-2462"
								class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2462"><a
									class="js-target-scroll" href="#">About Us</a></li>
							<li id="menu-item-2464"
								class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2464"><a
									class="js-target-scroll" href="#">Privacy Policy</a></li>
							<li id="menu-item-2394"
								class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2394"><a
									class="js-target-scroll" href="#">Site Map</a></li>
							<li id="menu-item-2395"
								class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2395"><a
									class="js-target-scroll" href="#">FAQs</a></li>
							<li id="menu-item-2463"
								class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2463"><a
									class="js-target-scroll" href="#">Contact Us</a></li>
						</ul>
					</div>
				</div>
				

				<div class="col-md-4 footer-news">

					<h5 class="social-title">Social Links</h5>
					<ul class="social_links">
						<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
						<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
						<li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
					</ul>

				</div>

			</div>

		</div>
	</footer>
	<!--/Footer-->
	<footer class="secondary-bg footer-sd">
		<div class="container">
			<div class="row">
				<div class="col-md-3">
					<div class="logo">
						<h3><a href="#"><span>S</span>M<span>S</span></a></h3>
					</div>
				</div>
				<div class="col-md-9 copyright-text">
					Copyright © 2023 chidera. All Rights Reserved </div>
			</div>
		</div>
	</footer>
	<!--Back to top-->
	<div id="back-top" class="back-top"> <a href="#top"><i class="fa fa-angle-up" aria-hidden="true"></i> </a> </div>
	<!--/Back to top-->
	